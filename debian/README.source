Original sources for the data in this package are as follows:

data/AustralianElectionPolling.rda:
Compiled from various media sources by Simon Jackman and Andrea Abel.

data/AustralianElections.rda:
Australian Electoral Commission. http://www.aec.gov.au/

data/EfronMorris.rda:
Efron, Bradley and Carl Morris. 1975. "Data Analysis Using Stein's
Estimator and Its Generalizations." Journal of the American Statistical
Association 70:311-319.

data/RockTheVote.rda:
Green, Donald P. and Lynn Vavreck. 2008. "Analysis of
Cluster-Randomized Experiments: A Comparison of Alternative Estimation
Approaches." Political Analysis 16:138-152.

data/UKHouseOfCommons.rda:
Jonathan Katz; Gary King. 1999. "Replication data for: A Statistical
Model of Multiparty Electoral Data", http://hdl.handle.net/1902.1/QIGTWZYTLZ

data/absentee.rda:
Ashenfelter, Orley.  1994.  "Report on Expected Asbentee Ballots."
Typescript.  Department of Economics, Princeton University.

data/admit.rda:
Compiled by the package author; replication data for:
Jackman, Simon.  2004.  "What Do We Learn From Graduate
Admissions Committees?: A Multiple-Rater, Latent Variable Model, with
Incomplete Discrete and Continuous Indicators." Political Analysis
12(4):400-424.

data/bioChemists.rda:
Replication data for: Long, J. Scott. 1990. "The origins of sex
differences in science." Social Forces 68(3):1297-1316.

data/ca2006.rda:
2006 data from the California Secretary of State's web site,
  http://www.sos.ca.gov/elections/prior-elections/statewide-election-results/general-election-november-7-2006/statement-vote/
2004 and 2000 presidential vote in congressional districts from the
2006 Almanac of American Politics.
   
data/iraqVote.rda:
Keith Poole, 107th Senate Roll Call Data. https://voteview.com/static/data/out/votes/S107_votes.ord  The Iraq vote is vote number 617.

David Leip's Atlas of U.S. Presidential Elections.  http://uselectionatlas.org

data/nj07.rda:
  Keith Poole's web site: http://legacy.voteview.com/senate110.htm

  Originally scraped from the Senate's web site by Jeff Lewis.

  Josh Clinton compiled the list of \emph{National Journal} key votes.  

data/partycodes.rda:
Keith Poole's website: http://legacy.voteview.com/PARTY3.HTM

data/politicalInformation.rda:
  The National Election Studies (www.electionstudies.org). THE 2000
  NATIONAL ELECTION STUDY [dataset]. Ann Arbor, MI: University of
  Michigan, Center for Political Studies [producer and distributor].  

data/presidentialElections.rda:
David Leip's Atlas of U.S. Presidential Elections. http://uselectionatlas.org

data/prussian.rda:
  von Bortkiewicz, L. 1898. Das Gesetz der Kleinen Zahlen. Leipzig: Teubner. 

data/s109.rda:
  Keith Poole's web site: https://legacy.voteview.com/senate109.htm

  Originally scraped from the Senate's web site by Jeff Lewis (UCLA).
  
  Information identifying the votes is available at
  https://voteview.com/static/data/out/rollcalls/S109_rollcalls.csv

data/sc9497.rda
Harold J. Spaeth (1999). United States Supreme Court Judicial
Database, 1953-1997 Terms.  Ninth edition.  Inter-university
Consortium for Political and Social Research.  Ann Arbor, Michigan.
\url{http://www.icpsr.umich.edu/}

data/state.info.rda:
Various ICPSR codebooks. http://www.icpsr.umich.edu

data/unionDensity.rda:
Pryor, Frederic. 1973. Property and Industrial Organization in
  Communist and Capitalist Countries. Bloomington: Indiana University
  Press.
  
Stephens, John and Michael Wallerstein. 1991. "Industrial
  Concentration, Country Size and Trade Union Membership."  American
  Political Science Review 85:941-953.
  
Western, Bruce and Simon Jackman. 1994. "Bayesian Inference for
  Comparative Research." American Political Science Review 88:412-423.
  
Wilensky, Harold L. 1981. "Leftism, Catholicism, Democratic
  Corporatism: The Role of Political Parties in Recemt Welfare State
  Development." In The Development of Welfare States in Europe and
  America, ed. Peter Flora and Arnold J. Heidenheimer. New Brunswick:
  Transaction Books.

data/vote92.rda:

Alvarez, R. Michael and Jonathan Nagler. 1995. "Economics, issues and
the Perot candidacy: Voter choice in the 1992 Presidential election."
American Journal of Political Science 39:714-44.

Miller, Warren E., Donald R. Kinder, Steven J. Rosenstone and the
National Election Studies.  1999.  National Election Studies, 1992:
Pre-/Post-Election Study.  Center for Political Studies, University of
Michigan: Ann Arbor, Michigan.

Inter-University Consortium for Political and Social Research.  Study Number 1112.  http://dx.doi.org/10.3886/ICPSR01112

 -- Chris Lawrence <lawrencc@debian.org>, Sun,  1 Oct 2017 00:12:21 -0400
