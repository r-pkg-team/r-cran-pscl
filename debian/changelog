r-cran-pscl (1.5.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 01 Feb 2024 16:57:35 +0100

r-cran-pscl (1.5.5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 26 Jun 2023 16:06:04 +0200

r-cran-pscl (1.5.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Thu, 19 Mar 2020 11:23:20 +0100

r-cran-pscl (1.5.2-3) unstable; urgency=medium

  * Team upload.
  * Maintainer: Debian R Packages Maintainers
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Thu, 13 Dec 2018 23:07:24 +0100

r-cran-pscl (1.5.2-2) unstable; urgency=medium

  * Team upload.
  * Secure URI in watch file
  * cme fix dpkg-control
  * debhelper 11
  * Take over package into r-pkg team
  * Convert from cdbs to dh-r
  * Add valid canonical homepage
  * Testsuite: autopkgtest-pkg-r
  * Convert to DEP5

 -- Andreas Tille <tille@debian.org>  Mon, 12 Mar 2018 07:09:52 +0100

r-cran-pscl (1.5.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 10 Oct 2017 10:14:49 -0400

r-cran-pscl (1.5.1-2) unstable; urgency=medium

  * Update for R 3.4.2.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 04 Oct 2017 10:37:42 -0400

r-cran-pscl (1.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove obsolete homepage.

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 30 Sep 2017 23:42:48 -0400

r-cran-pscl (1.4.9-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Fri, 10 Apr 2015 21:40:56 -0400

r-cran-pscl (1.4.8-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 08 Mar 2015 17:30:30 -0400

r-cran-pscl (1.4.6-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 28 Aug 2014 02:00:34 -0400

r-cran-pscl (1.04.4-2) unstable; urgency=low

  * Add dependency on r-cran-colorspace.  Works around bug in r-cran-vcd.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 21 Apr 2013 23:06:56 -0400

r-cran-pscl (1.04.4-1) unstable; urgency=low

  * New upstream release.
  * Rebuild for R 3.0.

 -- Chris Lawrence <lawrencc@debian.org>  Sun, 31 Mar 2013 20:55:40 -0400

r-cran-pscl (1.03.10-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 31 Mar 2011 12:22:42 -0500

r-cran-pscl (1.03.6.1-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 22 Mar 2011 15:48:45 -0500

r-cran-pscl (1.03.6-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 15 Jan 2011 11:02:38 -0600

r-cran-pscl (1.03.5-1) unstable; urgency=low

  * New upstream release.
  * Add additional suggestions from DESCRIPTION.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 14 Apr 2010 01:26:59 -0500

r-cran-pscl (1.03.4-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Mon, 12 Apr 2010 20:58:32 -0500

r-cran-pscl (1.03.3-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 30 Mar 2010 20:22:35 -0500

r-cran-pscl (1.03-1) unstable; urgency=low

  * New upstream release

 -- Chris Lawrence <lawrencc@debian.org>  Mon, 16 Mar 2009 22:12:11 -0500

r-cran-pscl (1.02-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 21 Oct 2008 12:18:01 -0500

r-cran-pscl (1.00-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Fri, 18 Jul 2008 13:33:50 -0500

r-cran-pscl (0.97-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 17 Jul 2008 20:15:27 -0500

r-cran-pscl (0.96-1) unstable; urgency=low

  * New upstream release.
  * Bump minimum required R version to 2.3.0, per DESCRIPTION.
  * Copy copyright information from upstream COPYRIGHTS.
  * Point to GPL-2 instead of GPL, since the license is GPL v2 only.

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 05 Jul 2008 14:49:30 -0500

r-cran-pscl (0.95-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 29 Mar 2008 02:11:08 -0500

r-cran-pscl (0.94-1) unstable; urgency=low

  * New upstream release

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 16 Feb 2008 23:28:32 -0600

r-cran-pscl (0.92-2) unstable; urgency=low

  * Add ${shlibs:Depends} to pull in needed dependencies.

 -- Chris Lawrence <lawrencc@debian.org>  Tue, 10 Jul 2007 17:40:15 -0500

r-cran-pscl (0.92-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 14 Jun 2007 14:00:51 -0500

r-cran-pscl (0.75-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sat,  3 Feb 2007 21:18:46 -0600

r-cran-pscl (0.73-1) unstable; urgency=low

  * New upstream release

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 29 Nov 2006 22:54:19 -0600

r-cran-pscl (0.72-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 25 Oct 2006 14:29:56 -0500

r-cran-pscl (0.61-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Sat, 19 Aug 2006 12:55:48 -0500

r-cran-pscl (0.60-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Fri,  4 Aug 2006 17:31:53 -0500

r-cran-pscl (0.59-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 27 Jul 2006 14:00:30 -0400

r-cran-pscl (0.58-1) unstable; urgency=low

  * New upstream release.
  * Make arch-specific, since it now ships a binary module.

 -- Chris Lawrence <lawrencc@debian.org>  Wed, 12 Jul 2006 19:52:05 -0400

r-cran-pscl (0.54-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lawrence <lawrencc@debian.org>  Thu,  1 Jun 2006 16:17:17 -0400

r-cran-pscl (0.52-1) unstable; urgency=low

  * New upstream release.
  * Remove post{inst,rm}; bump dependencies accordingly.

 -- Chris Lawrence <lawrencc@debian.org>  Sat,  8 Oct 2005 20:04:52 -0400

r-cran-pscl (0.51-1) unstable; urgency=low

  * New upstream release.
  * Install HISTORY as the upstream changelog.

 -- Chris Lawrence <lawrencc@debian.org>  Thu, 23 Jun 2005 10:55:14 -0500

r-cran-pscl (0.5-1) unstable; urgency=low

  * Initial Debian Release.  (Closes: #312794)

 -- Chris Lawrence <lawrencc@debian.org>  Fri, 10 Jun 2005 02:09:00 -0500
